// function that creates a store
import createStore from '@app/shared/store/index'
// if you want a separate store definition for this webpage use:
// import createStore from './store/index'

// function that creates a router
import createRouter from '@app/shared/router/index'
// if you want a separate router definition for this webpage use:
// import createRouter from './router/index'

// get webpage routes
import routes from './router/routes'

import createApp from '@app/shared/js/app'
const { app, store, router } = createApp(createStore, createRouter, routes)

import bootFiles from '@app/shared/boot'
// if you wish to add boot files specific to this webpage use:
// import myCustomWebpageFunc from './boot/my-custom-webpage-code'
// bootFiles.push(myCustomWebpageFunc)

import start from '@app/shared/js/start'
start(bootFiles, app, store, router)
