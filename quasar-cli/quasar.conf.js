// Configuration for your app
// https://quasar.dev/quasar-cli/quasar-conf-js

const HtmlWebpackPlugin = require('html-webpack-plugin')
const AutoChunksWebpackPlugin = require('./webpack-plugins/autochunks')
const path = require('path')
const params = require('./params')
const pages = require(`./apps/${params.app}/pages`)

module.exports = function (ctx) {
  return {
    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    // https://quasar.dev/quasar-cli/cli-documentation/boot-files
    boot: [
      'i18n',
      'axios'
    ],

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-css
    css: [
      'app.sass'
    ],

    // https://github.com/quasarframework/quasar/tree/dev/extras
    extras: [
      // 'ionicons-v4',
      // 'mdi-v4',
      // 'fontawesome-v5',
      // 'eva-icons',
      // 'themify',
      // 'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!

      'roboto-font', // optional, you are not bound to it
      'material-icons' // optional, you are not bound to it
    ],

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-framework
    framework: {
      // iconSet: 'ionicons-v4', // Quasar icon set
      // lang: 'de', // Quasar language pack

      // Possible values for "all":
      // * 'auto' - Auto-import needed Quasar components & directives
      //            (slightly higher compile time; next to minimum bundle size; most convenient)
      // * false  - Manually specify what to import
      //            (fastest compile time; minimum bundle size; most tedious)
      // * true   - Import everything from Quasar
      //            (not treeshaking Quasar; biggest bundle size; convenient)
      all: 'auto',

      components: [],
      directives: [],

      // Quasar plugins
      plugins: []
    },

    // https://quasar.dev/quasar-cli/cli-documentation/supporting-ie
    supportIE: false,

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-build
    build: {
      scopeHoisting: true,
      // vueRouterMode: 'history',
      // showProgress: false,
      // gzip: true,
      // analyze: true,
      // preloadChunks: false,
      // extractCSS: false,

      // https://quasar.dev/quasar-cli/cli-documentation/handling-webpack
      extendWebpack (cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules/,
          options: {
            formatter: require('eslint').CLIEngine.getFormatter('stylish')
          }
        })

        cfg.resolve.alias = {
          ...cfg.resolve.alias,
          '@apps': `${__dirname}/apps`,
          '@app': `${__dirname}/apps/${params.app}`
        }

        // Rule for loading html files with '?external' resourceQuery.
        // Files will be processed and output as separate files in the output directory.
        cfg.module.rules.push({
          test: /\.html$/,
          resourceQuery: /external/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'html/[path][name].wp.html',
                context: `apps/${params.app}`
              }
            },
            { loader: 'extract-loader' },
            { loader: 'html-loader' }
          ]
        })

        // Rules for loading pug files
        cfg.module.rules.push({
          test: /\.pug$/,
          oneOf: [
            {
              resourceQuery: /vue/,
              use: [{ loader: 'pug-plain-loader' }]
            },
            {
              // Files with '?external' resourceQuery will be processed and output as separate files.
              resourceQuery: /external/,
              exclude: [/webpack-html-templates/],
              use: [
                {
                  loader: 'file-loader',
                  options: {
                    name: 'html/[path][name].wp.html',
                    context: `apps/${params.app}`
                  }
                },
                { loader: 'extract-loader' },
                { loader: 'html-loader' },
                { loader: 'pug-plain-loader' }
              ]
            },
            {
              use: [{ loader: 'raw-loader' }, { loader: 'pug-plain-loader' }]
            }
          ]
        })

        if (ctx.mode.spa || ctx.mode.pwa) {
          // Add entry for html/pug files
          // Use 'html_files.js' to import html files that you want to be output
          // as separate files, ex: import './layout.pug?external
          cfg.entry.html_files = `${__dirname}/apps/${params.app}/html_files.js`

          Object.keys(pages).forEach(function (page) {
          // Add page webpack entry
            cfg.entry[page] = pages[page].entry

            // HtmlWebpackPlugin default output file
            let defaultPageFilename =
            'html/' +
            path.relative(
              `${__dirname}/apps/${params.app}`,
              path.dirname(pages[page].entry)
            ) +
            `/${page}.wp.html`

            // if page template and filename are not provided, use defaults
            pages[page].template = pages[page].template
              ? pages[page].template
              : params.indexHtmlTemplate
            pages[page].filename = pages[page].filename
              ? pages[page].filename
              : defaultPageFilename

            // Output an html file for the page with all necessary assets injected
            cfg.plugins.push(
              new HtmlWebpackPlugin({
                template: pages[page].template,
                filename: pages[page].filename,
                chunks: [page]
              })
            )
          })

          // Auto add necessary chunks to HtmlWebpackPlugin generated files
          cfg.plugins.push(
            new AutoChunksWebpackPlugin()
          )

          // Transform links in HtmlWebpackPlugin generated html files to django format
          // ex: {% static 'bundles/myapp/js/app.4ddb1544.js' %}
          if (params.djangoStaticTags) {
            const DjangoStaticWebpackPlugin = require('django-static-webpack-plugin')
            cfg.plugins.push(
              new DjangoStaticWebpackPlugin({
                bundlePath: `bundles/${params.app}`,
                excludeFilenames: ['index.html']
              })
            )
          }
        }
      },

      chainWebpack (chain) {
        if (ctx.mode.spa || ctx.mode.pwa) {
          // Restrict chunks in 'index.html'
          chain.plugin('html-webpack').tap(args => {
            args[0].chunks = ['app']
            return args
          })
        }

        if (!ctx.prod && !ctx.mode.ssr) {
          chain.output
            .path(params.distDir)
            .publicPath(params.publicPath)
        }
      },

      distDir: params.distDir,
      publicPath: params.publicPath,
      vueCompiler: true,
      rtl: params.rtl
    },

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-devServer
    devServer: {
      publicPath: params.publicPath,

      // Proxy requests for non webpack assets
      proxy: [
        {
          context: ['!' + params.publicPath + '**'],
          target: params.devServerProxyTarget
        }
      ],

      // Write html files to disk as they might be needed by other servers
      writeToDisk: filePath => {
        return /\.html$/.test(filePath)
      },
      // https: true,
      // port: 8080,
      open: true // opens browser window automatically
    },

    // animations: 'all', // --- includes all animations
    // https://quasar.dev/options/animations
    animations: [],

    // https://quasar.dev/quasar-cli/developing-ssr/configuring-ssr
    ssr: {
      pwa: false
    },

    // https://quasar.dev/quasar-cli/developing-pwa/configuring-pwa
    pwa: {
      // workboxPluginMode: 'InjectManifest',
      // workboxOptions: {}, // only for NON InjectManifest
      manifest: {
        // name: 'Quasar App',
        // short_name: 'Quasar App',
        // description: 'A Quasar Framework app',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            'src': 'statics/icons/icon-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },

    // https://quasar.dev/quasar-cli/developing-cordova-apps/configuring-cordova
    cordova: {
      // id: 'org.cordova.quasar.app',
      // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
    },

    // https://quasar.dev/quasar-cli/developing-electron-apps/configuring-electron
    electron: {
      // bundler: 'builder', // or 'packager'

      extendWebpack (cfg) {
        // do something with Electron main process Webpack cfg
        // chainWebpack also available besides this extendWebpack
      },

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Windows only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration

        // appId: 'quasar-cli'
      }
    }
  }
}
