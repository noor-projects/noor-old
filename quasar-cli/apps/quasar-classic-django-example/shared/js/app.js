import Vue from 'vue'

export default function (createStore, createRouter, routes) {
  // create store and router instances

  const store = typeof createStore === 'function'
    ? createStore({ Vue })
    : createStore

  const router = typeof createRouter === 'function'
    ? createRouter(routes)
    : createRouter

  // make router instance available in store
  store.$router = router

  // Create the app instantiation Object.
  // Here we inject the router, store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    el: '#q-app',
    router,
    store,
    delimiters: ['[[', ']]']
  }

  // expose the app, the router and the store.
  // note we are not mounting the app here, since bootstrapping will be
  // different depending on whether we are in a browser or on the server.
  return {
    app,
    store,
    router
  }
}
