import Vue from 'vue'
import { Quasar } from 'quasar'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

export default async ({ app }) => {
  const locale = Quasar.lang.isoName

  await import(
    /* webpackInclude: /(ar|en-us)\/index\.js$/ */
    /* webpackChunkName: "vue-i18n-[request]" */
    `@app/shared/i18n/${locale}`
  )
    .then(messages => {
      const i18n = new VueI18n({
        locale,
        fallbackLocale: locale,
        messages: messages.default
      })

      // Set i18n instance on app
      app.i18n = i18n
    })
    .catch(err => {
      console.error('[Noor] error importing VueI18n messages:', err)
    })
}
