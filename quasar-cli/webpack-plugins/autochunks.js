// A plugin that finds automatically the needed chunks to include in an HTMLWebpackPlugin instance

class AutoChunksWebpackPlugin {
  apply (compiler) {
    compiler.hooks.compilation.tap('AutoChunksWebpackPlugin', compilation => {
      compilation.hooks.htmlWebpackPluginAlterChunks.tap(
        'AutoChunksWebpackPlugin',
        (_, { plugin }) => {
          const mainChunk = plugin.options.chunks[0]
          const entrypoint = compilation.entrypoints.get(mainChunk)

          return entrypoint.chunks.map(chunk =>
            ({
              names: chunk.name ? [chunk.name] : [],
              files: chunk.files.slice(),
              size: chunk.modulesSize(),
              hash: chunk.hash
            })
          )
        }
      )
    })
  }
}

module.exports = AutoChunksWebpackPlugin
