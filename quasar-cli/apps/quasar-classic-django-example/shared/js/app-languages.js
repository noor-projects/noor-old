// mapping between backend languages and quasar languages
export const qLangs = {
  // list your app languages here
  ar: 'ar',
  en: 'en-us'
}

// function that imports a quasar lang pack based on a provided backend lang
export async function importQLangPack (backendLang) {
  try {
    const qLangPack = await import(
      // modify the list of your app languages in the comment below
      /* webpackInclude: /(ar|en-us)\.js$/ */
      /* webpackChunkName: "lang-pack-[request]" */
      'quasar/lang/' + qLangs[backendLang]
    )
    return qLangPack
  } catch (err) {
    console.error('[Quasar] error importing quasar language pack:', err)
  }
}
