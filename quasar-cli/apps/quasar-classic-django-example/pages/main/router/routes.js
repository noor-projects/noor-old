export default [
  {
    path: '/',
    component: () => import('@app/shared/layout/AppLayout.vue'),
    children: [
      {
        path: '',
        components: {
          header: () => import('@app/shared/layout/AppHeader.vue'),
          'primary-drawer': () => import('@app/shared/layout/AppPrimaryDrawer.vue'),
          page: () => import('@app/pages/main/pages/Index.vue')
        }
      }
    ]
  }
]
