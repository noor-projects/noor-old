// this file is similar to the auto-generated './quasar/client-entry.js' for SPA

import '@quasar/extras/roboto-font/roboto-font.css'
import '@quasar/extras/material-icons/material-icons.css'

// We load Quasar stylesheet file
import 'quasar/dist/quasar.sass'

import '@app/shared/css/app.sass'

if (process.env.PROD && process.env.MODE === 'pwa') {
  import(
    /* webpackChunkName: "service-worker" */
    'app/src-pwa/register-service-worker.js'
  )
}

// Needed only for iOS PWAs
if (
  /iPad|iPhone|iPod/.test(navigator.userAgent) &&
  !window.MSStream &&
  window.navigator.standalone
) {
  import(/* webpackChunkName: "fastclick"  */ '@quasar/fastclick')
}

import Vue from 'vue'
import initQuasar from './import-quasar.js'

export default async function (bootFiles, app, store, router) {
  await initQuasar()

  let routeUnchanged = true
  const redirect = url => {
    routeUnchanged = false
    window.location.href = url
  }

  const urlPath = window.location.href.replace(window.location.origin, '')

  // eslint-disable-next-line no-unmodified-loop-condition
  for (let i = 0; routeUnchanged === true && i < bootFiles.length; i++) {
    if (typeof bootFiles[i] !== 'function') {
      continue
    }

    try {
      await bootFiles[i]({
        app,
        router,
        store,
        Vue,
        ssrContext: null,
        redirect,
        urlPath
      })
    } catch (err) {
      if (err && err.url) {
        window.location.href = err.url
        return
      }

      console.error('[Quasar] boot error:', err)
      return
    }
  }

  if (routeUnchanged === false) {
    return
  }

  new Vue(app).$mount('#q-app')
}
