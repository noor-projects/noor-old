import Vue from 'vue'
import Vuex from 'vuex'

import main from './main'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      main
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  if (process.env.DEV && module.hot) {
    module.hot.accept(['./main'], () => {
      const newMain = require('./main').default
      Store.hotUpdate({ modules: { main: newMain } })
    })
  }

  return Store
}
