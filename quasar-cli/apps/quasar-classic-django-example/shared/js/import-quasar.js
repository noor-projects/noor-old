import Vue from 'vue'
import { Quasar } from 'quasar'

import { qLangs, importQLangPack } from './app-languages'

export default async function () {
// before initializing quasar we get the backend language from
// the html 'lang' attribute
  const backendLang = document.documentElement.getAttribute('lang')

  if (qLangs[backendLang] === 'en-us') {
  // if quasar language is 'en-us' we don't need to import its lang pack
  // because it is the default.
    Vue.use(Quasar)
  } else {
    const qLangPack = await importQLangPack(backendLang)
    if (qLangPack) {
      // initialize quasar with the imported language pack
      Vue.use(Quasar, { lang: qLangPack.default })
    } else {
      console.error('[Quasar] error loading quasar language pack')
      // initialize quasar without a language pack (defaults to 'en-us')
      Vue.use(Quasar)
    }
  }
}
